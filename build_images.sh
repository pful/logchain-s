#!/bin/bash

docker build -t pful/logchain-s-trust-node:dev2 -f trust_peer.dockerfile .
docker build -t pful/logchain-s-generic-node:dev2 -f generic_peer.dockerfile .
