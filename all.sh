#!/bin/bash

source ./stop_containers.sh
source ./remove_containers.sh
source ./build_images.sh
source ./start_containers.sh
