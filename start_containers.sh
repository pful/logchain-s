#!/bin/bash

docker run --name trust-node -d -v /Users/daeyeonjoo/Projects/logchain-s/confs/trust:/conf --net logchainnet --ip 10.50.0.10 -p 5002:5000 pful/logchain-s-trust-node:dev2
docker run --name generic-node1 -d -v /Users/daeyeonjoo/Projects/logchain-s/confs/generic1:/conf --net logchainnet --ip 10.50.0.30 -p 5000:5000 pful/logchain-s-generic-node:dev2
docker run --name generic-node2 -d -v /Users/daeyeonjoo/Projects/logchain-s/confs/generic2:/conf --net logchainnet --ip 10.50.0.31 -p 5001:5000 pful/logchain-s-generic-node:dev2
